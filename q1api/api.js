"use strict";
// Guang Zhang 1942372
// JavaScript: test2 question 1 API

/**
 * retrieve nutritions info from the nutrition API, add the nutrition info to the section in html
 * API: https://sonic.dawsoncollege.qc.ca/~jaya/old/foodapi.php
 * create a url + params & retrieve
 * @author Guang
 * @version 2020-11-23
 */

 // initialize the globals in the global document
let globals = {};
document.addEventListener("DOMContentLoaded", setup);
// define section again in the setup function and add 'click' to the button for getting nutrition info from the api
function setup(){
    // fire getNutrition function on the 'submit' event.
    // fired on the from to use html5 validation
    document.querySelector('form').addEventListener('submit',getNutrition);
    globals.apiURL = "https://sonic.dawsoncollege.qc.ca/~jaya/old/foodapi.php";
    globals.section = document.querySelector('section');
    globals.search = document.querySelector('#search');
}

/**
 * Read the form Search item and construct the url 
 * Retrieve the Nutrition info from the API
 * @param {*} e 
 */
function getNutrition(e) {
    e.preventDefault();
    let url = constructURL();
    getJSON(url, addNutrition);

}

/**
 * Construct the URL for retrieving from the API with the search value
 */
function constructURL() {
    let url = new URL(globals.apiURL);
    // construct params querystring ?search=value
    url.searchParams.set("search", globals.search.value);
    console.log(url);
    return url;
}

/**
 * Given JSON data with the search in text
 * Create p element and associtate with the JSON data as its textContent
 * Add the p element to the section in the html document
 * @param {JSON} jsondata
 */
function addNutrition(jsondata) {
    console.log(jsondata);
    let p = document.createElement('p');
    globals.section.appendChild(p);
    p.textContent = `Nutrition calories: ${jsondata.nutrition[0].calories} ; Category: ${jsondata.category} ; Number of names (count): ${jsondata.names.length}`;
    
}

/**
 * Retrieve json from the nutrition api and perform action on it
 * Display error message if the search item is not supported by the API
 * @param {string} uri 
 * @param {function to read JSON} action 
 */
function getJSON(uri, action) {
    fetch(uri)
        .then(response => {
            if (!response.ok){
                throw new Error(`status: ${response.status}`);
            } else {
                return response.json();
            }
        }).then(json => action(json))
            .catch (e =>
                { console.log('Error: please try again in the paragraph in the section.');
                    console.log(e.message+" "+uri);
                    let p = document.createElement('p');
                    globals.section.appendChild(p);
                    p.textContent = "Error: please try again in the paragraph in the section.";

                });
}