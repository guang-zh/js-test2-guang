"use strict";
// Guang Zhang 1942372
// JavaScript: test2 question 2 animate
 
// initialize the img, buttons in the global document
let image; 
let count;
let createClock;

document.addEventListener("DOMContentLoaded", setup);
// define img again in the setup function 
function setup(){
    count = 0;
    image = document.querySelector('img');
    document.querySelector("#start").addEventListener('click', startSwap);
    document.querySelector("#stop").addEventListener('click', stopSwap);
    console.log("setup done");
}

/**
 * Start swapping images every 800 milliseconds
 * @param {event} e 
 */
function startSwap(e) {
    e.preventDefault();
    createClock = setInterval(swapImage, 800);
}

/**
 * Swap images
 * @param {event} e 
 */
function swapImage() {
    if (count%3==0){
        image.src="https://sonic.dawsoncollege.qc.ca/~jaya/old/0.jpg"
    } else if (count%3==1){
        image.src="https://sonic.dawsoncollege.qc.ca/~jaya/old/1.jpg";
    } else if (count%3==2){
        image.src="https://sonic.dawsoncollege.qc.ca/~jaya/old/2.jpg";
    }
    count++;
}

/**
 * Stop Swapping images 
 * @param {event} e 
 */
function stopSwap(e) {
    clearInterval(createClock);
}
